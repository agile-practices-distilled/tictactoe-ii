using System.Linq;

namespace TicTacToe_II
{
    public class Panel
    {
        private readonly string[,] _panel;

        public Panel()
        {
            _panel = new[,]
            {
                {"_", "_", "_"},
                {"_", "_", "_"},
                {"_", "_", "_"}
            };
        }
        
        public void AddChip(Position position, ChipTracker chipTracker)
        {
            var row = 0;

            while (IsPositionOccupied(position, row) && IsInsidePanel(row))
            {
                row++;
            }
            
            _panel[row, position.Column] = chipTracker.GetNextChip();
        }

        public bool HasAnyPlayerWon()
        {
            return IsHorizontalVictory() || 
                   IsVerticalVictory() || 
                   IsDiagonalVictory();
        }

        private bool IsHorizontalVictory()
        {
            return _panel[0, 0] != "_" && _panel[0, 0] == _panel[0, 1] && _panel[0, 0] == _panel[0, 2] ||
                   _panel[1, 0] != "_" && _panel[1, 0] == _panel[1, 1] && _panel[1, 0] == _panel[1, 2] ||
                   _panel[2, 0] != "_" && _panel[2, 0] == _panel[2, 1] && _panel[2, 0] == _panel[2, 2];
        }

        private bool IsVerticalVictory()
        {
            return _panel[0, 0] != "_" && _panel[0, 0] == _panel[1, 0] && _panel[0, 0] == _panel[2, 0] ||
                   _panel[0, 1] != "_" && _panel[0, 1] == _panel[1, 1] && _panel[0, 1] == _panel[2, 1] ||
                   _panel[0, 2] != "_" && _panel[0, 2] == _panel[1, 2] && _panel[0, 2] == _panel[2, 2];
        }

        private bool IsDiagonalVictory()
        {
            return _panel[1,1] != "_" && _panel[0,2] == _panel[1,1] && _panel[1,1] == _panel[2,0] ||
                   _panel[1,1] != "_" && _panel[2,2] == _panel[1,1] && _panel[1,1] == _panel[0,0];
        }

        public bool IsDraw()
        {
            return Enumerable.Range(0, _panel.GetUpperBound(0)+1)
                .All(r => _panel[r,1] != "_");
        }

        private static bool IsInsidePanel(int row)
        {
            return row < 3;
        }

        private bool IsPositionOccupied(Position position, int row)
        {
            return _panel[row, position.Column] != "_";
        }

        public override string ToString()
        {
            return $"|{_panel[2,0]}|{_panel[2,1]}|{_panel[2,2]}|" +
                   $"|{_panel[1,0]}|{_panel[1,1]}|{_panel[1,2]}|" +
                   $"|{_panel[0,0]}|{_panel[0,1]}|{_panel[0,2]}|";
        }
    }
}