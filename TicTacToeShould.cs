using NUnit.Framework;

namespace TicTacToe_II
{
    public class TicTacToeShould
    {
        private TicTacToe _ticTacToe;

        [SetUp]
        public void Init()
        {
            _ticTacToe = new TicTacToe();
        }
        
        [TestCase(0,"|_|_|_|" +
                    "|_|_|_|" +
                    "|X|_|_|")]
        [TestCase(1,"|_|_|_|" +
                    "|_|_|_|" +
                    "|_|X|_|")]
        [TestCase(2,"|_|_|_|" +
                    "|_|_|_|" +
                    "|_|_|X|")]
        public void add_chip_in_any_position(int position, string expectedResult)
        {
            _ticTacToe.AddChip(new Position(position));
            
            Assert.AreEqual(expectedResult, _ticTacToe.ToString());
        }

        [Test]
        public void insert_chip_on_top_when_occupied()
        {
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(1));
            
            Assert.AreEqual("" +
                            "|_|_|_|" +
                            "|_|O|_|" +
                            "|_|X|_|", _ticTacToe.ToString());
        }

        [Test]
        public void insert_chip_three_times_same_position()
        {
            _ticTacToe.AddChip(new Position(2));
            _ticTacToe.AddChip(new Position(2));
            _ticTacToe.AddChip(new Position(2));
            
            Assert.AreEqual("" +
                            "|_|_|X|" +
                            "|_|_|O|" +
                            "|_|_|X|", _ticTacToe.ToString());
        }

        [Test]
        public void notify_victory_when_3_chips_in_a_row_horizontally()
        {
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(2));
            
            Assert.AreEqual("VICTORY!" +
                            "|O|_|_|" +
                            "|O|_|_|" +
                            "|X|X|X|", _ticTacToe.ToString());
        }

        [Test]
        public void notify_victory_when_3_chips_in_a_row_vertically()
        {
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(0));
            
            Assert.AreEqual("VICTORY!" +
                            "|X|_|_|" +
                            "|X|O|_|" +
                            "|X|O|_|", _ticTacToe.ToString());
        }

        [Test]
        public void notify_victory_when_3_chips_in_a_row_diagonally()
        {
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(2));
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(1));
            
            Assert.AreEqual("VICTORY!" +
                            "|O|_|_|" +
                            "|X|O|_|" +
                            "|X|X|O|", _ticTacToe.ToString());
        }

        [Test]
        public void notify_draw_when_squares_filled()
        {
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(2));
            _ticTacToe.AddChip(new Position(0));
            _ticTacToe.AddChip(new Position(2));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(1));
            _ticTacToe.AddChip(new Position(2));
            _ticTacToe.AddChip(new Position(0));
            
            Assert.AreEqual("DRAW!" +
                            "|X|X|O|" +
                            "|O|O|X|" +
                            "|X|O|X|", _ticTacToe.ToString());
        }
    }
}