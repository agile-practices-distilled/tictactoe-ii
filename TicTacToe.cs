﻿using System;

namespace TicTacToe_II
{
    public class TicTacToe
    {
        private readonly Panel _state;
        private readonly ChipTracker _chipTracker;

        public TicTacToe()
        {
            _chipTracker = new ChipTracker();
            _state = new Panel();
        }
        
        public void AddChip(Position position)
        {
            _state.AddChip(position, _chipTracker);
        }

        public override string ToString()
        {
            string result = "";
            if (_state.HasAnyPlayerWon())
                result += "VICTORY!";
            
            if (_state.IsDraw())
                result += "DRAW!";
            
            return result += _state.ToString();
        }
    }
}