namespace TicTacToe_II
{
    public class ChipTracker
    {
        private string _currentChip;

        public ChipTracker(string currentChip = null)
        {
            if (string.IsNullOrEmpty(currentChip))
                currentChip = "O";
            
            _currentChip = currentChip;
        }

        public string GetNextChip()
        {
            var chip = "O";
            if (_currentChip == "O")
                chip = "X";

            _currentChip = chip;

            return chip;
        }
    }
}