namespace TicTacToe_II
{
    public class Position
    {
        public readonly int Column;

        public Position(int column)
        {
            Column = column;
        }
    }
}